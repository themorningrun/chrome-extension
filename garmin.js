(function() {
  'use strict'

  // Set to true for debug mode
  var DEBUG = false

  // Global Constants
  var WORKOUT_EDIT_PAGE = 'body-workouts body-workout body-workout-edit',
  WORKOUT_DETAILS_PAGE = 'body-workouts body-workout body-page-top',
  SEND_TO_APP = 'themorning.run.send-to-app',
  SHOW_COPY_URL = 'themorning.run.show-copy-url',
  EXTENSION_LINK = 'https://chrome.google.com/webstore/detail/themorningrun-garmin-work/ecohbeclccbmibphmcoomgoanpdjkiog',
  GARMIN = {
    RUN_URL: 'https://connect.garmin.com/modern/workout/create/running',
    BIKE_URL: 'https://connect.garmin.com/modern/workout/create/cycling',
    OTHER_URL: 'https://connect.garmin.com/modern/workout/create/other',
    IMPORT_PARAM: 'mri',
  },
  KEYS = {}

  if (DEBUG) {
    KEYS = {
      DURATION: 'duration',
      NAME: 'name',
      TEXT: 'text',
      TYPE: 'type',
      UNIT: 'unit',
      VALUE: 'value',
      TARGET: 'target',
      FROM: 'from',
      TO: 'to',
      STEPS: 'steps',
      RANGE: 'range',
      COUNT: 'count',
      TARGET_TYPE: 'target_type'
    }
  } else {
    KEYS = {
      DURATION: 'd',
      NAME: 'n',
      TEXT: 't',
      TYPE: 'y',
      UNIT: 'u',
      VALUE: 'v',
      TARGET: 'g',
      FROM: 'f',
      TO: 'o',
      STEPS: 's',
      RANGE: 'r',
      COUNT: 'c',
      TARGET_TYPE: 'e',
    }
  }

  // Scopes
  var helpers

  initialize()

  /**
  * Initializer function. Adds a watcher on the body to detect when the page
  * content has changed.
  */
  function initialize() {
    if (DEBUG) console.log('TMR: Initializing.')
    // Garmin changes a `data-body-level-class` attribute on the `<body>` so we
    // can use that to figure out when we should run our code.
    new MutationObserver(determineAction)
      .observe(document.body, {
          attributes: true,
          attributeFilter: [ 'class' ],
        }
      )
  }

  /**
  * Checks DOM state to determine which action should be taken.
  */
  function determineAction(mutation) {
    var currentState = document.body.classList
    if (DEBUG) console.log('TMR: current state: ', currentState)

    var isDetailsPage = WORKOUT_DETAILS_PAGE
      .split(' ')
      .reduce(function(isTrue, className) {
        return isTrue && currentState.contains(className)
      }, true)

    var isEditPage = WORKOUT_EDIT_PAGE
      .split(' ')
      .reduce(function(isTrue, className) {
        return isTrue && currentState.contains(className)
      }, true)

    if (isDetailsPage) {
      addSaveWorkoutLink()
      if (sessionStorage.getItem(SHOW_COPY_URL)) {
        displayURL()
      }
    } else if (isEditPage) {
      handleImportExport()
    }
  }

  /**
  * Handles the import/export of a workout.
  */
  function handleImportExport() {
    if (DEBUG) console.log('TMR: Handling Import or Export.')
    if (sessionStorage.getItem(SEND_TO_APP)) {
      exportWorkout()
    } else {
      var workout = helpers.getURLParam(GARMIN.IMPORT_PARAM)
      if (workout) {
        importWorkout(JSON.parse(decodeURIComponent(workout)))
      } else {
        if (DEBUG) console.log('TMR: No workout param found.')
      }
    }
  }

  function exportWorkout() {
    if (DEBUG) console.log('TMR: Exporting Workout.')
    var importURL, workout, baseURL

    if (document.querySelector('.icon-activity-running')) {
      baseURL = GARMIN.RUN_URL
    } else if (document.querySelector('.icon-activity-cycling')) {
      baseURL = GARMIN.BIKE_URL
    } else if (document.querySelector('.icon-activities')) {
      baseURL = GARMIN.OTHER_URL
    } else {
      return
    }
    // Grab the workout data.
    workout = copyWorkout()
    // Build the import URL.
    importURL = baseURL + '?' + GARMIN.IMPORT_PARAM + '=' + encodeURIComponent(JSON.stringify(workout))
    if (DEBUG) console.log('TMR: ', importURL)
    // Remove the record that exporting is ocurring.
    sessionStorage.removeItem(SEND_TO_APP)
    sessionStorage.setItem(SHOW_COPY_URL, importURL)
    // Now go back to the list of workouts.
    history.go(-2)
  }


  function displayURL() {
    if (DEBUG) console.log('TMR: Displaying url.')
    var url = sessionStorage.getItem(SHOW_COPY_URL)
    if (!url) return
    var shareLink = document.querySelector('a.send-to-the-morning-run'),
    successMessage = document.createElement('div')

    successMessage.className = 'alert alert-success'
    successMessage.innerHTML = '<h4>Almost there!</h4> Just copy the link below and share it with your friend.<br/><br/>' +
    '<input type="text" value="' + url + '" id="themorningrun-share-input" style="width: 100%;" /><br/><br/>' +
    '<small class="text-center">Be sure that they have <a href="' + EXTENSION_LINK + '" target="_blank" class="colored">TheMorning.Run Chrome extension installed</a>.</small>'

    shareLink.parentNode.appendChild(successMessage)
    document.querySelector('#themorningrun-share-input').select()

    sessionStorage.removeItem(SHOW_COPY_URL)
  }

  /**
  * Adds the Share Workout link to the list of actions on the workout details.
  */
  function addSaveWorkoutLink() {
    var sendToDeviceLink = document.querySelector('.colored.send-to-device'),
    newListItem = document.createElement('li'),
    newLink = document.createElement('a'),
    newLinkText = document.createTextNode('Share With a Friend')

    if (document.querySelector('.icon-activity-swimming')) {
      return
    }

    newLink.appendChild(newLinkText)

    newLink.className = 'colored send-to-the-morning-run'
    newLink.href = '#'
    newLink.onclick = onSendToAppButtonClick

    newListItem.appendChild(newLink)

    sendToDeviceLink.parentNode.parentNode
      .insertBefore(newListItem, sendToDeviceLink.parentNode)
  }

  /**
  * onClick action handler for the Share Workout button.
  */
  function onSendToAppButtonClick() {
    sessionStorage.setItem(SEND_TO_APP, true)
    var editButton = document.querySelector('.btn.edit-workout')
    if (editButton) editButton.click()
  }

  /**
  * Parses the workout data on the current page and returns the JSON data for it.
  */
  function copyWorkout() {
    var titleField = document.querySelector('#workout-name-edit .inline-edit-target'),
    workoutEditor = document.querySelector('.workout-editor'),
    steps = [],
    workout = {}

    for (var i = 0; i < workoutEditor.children.length; i++) {
      var step = workoutEditor.children[i]
      if (step.classList.contains('workout-step')) {
        steps.push(parseStep(step))
      } else if (step.classList.contains('block-repeat')) {
        steps.push(parseBlock(step))
      }
    }

    if (titleField) workout[KEYS.NAME] = titleField.innerText
    workout[KEYS.STEPS] = steps
    return workout
  }

  function parseStep(step) {
    var type = step.querySelector('select[name="select-step-type"]'),
    duration = step.querySelector('.step-duration'),
    target = step.querySelector('.step-target'),
    parsed = {}

    parsed[KEYS.TYPE] = parseFieldValue(type)
    parsed[KEYS.DURATION] = parseDuration(duration)
    parsed[KEYS.TARGET] = parseTarget(target)
    return parsed
  }

  function parseBlock(block) {
    var count = block.querySelector('.repeat-count'),
    steps = block.querySelectorAll('.workout-step'),
    blockSteps = [],
    parsed = {}

    for (var i = 0; i < steps.length; i++) {
      blockSteps.push(parseStep(steps[i]))
    }

    parsed[KEYS.COUNT] = count ? parseInt(count.innerText) : 1
    parsed[KEYS.STEPS] = blockSteps
    return parsed
  }

  function parseFieldValue(field) {
    if (!field) return null
    var parsed = {}
    if (field.getAttribute('name')) parsed[KEYS.NAME] = field.getAttribute('name').trim()
    if (field.text || field.innerText) parsed[KEYS.TEXT] = field.text.trim() || field.innerText.trim()
    if (field.options) parsed[KEYS.TEXT] = field.options[field.selectedIndex].innerText.trim()
    if (field.value) parsed[KEYS.VALUE] = field.value.trim()
    return parsed
  }

  function parseDuration(duration) {
    var type = duration.querySelector('select[name="duration"] option[selected]'),
    parsed = {},
    unit,
    range

    if (!type || !type.value) return
    type = type.value
    switch(type) {
      case 'time':
        unit = duration.querySelector('.add-on')
        unit = unit ? unit.innerText : null
        parsed = parseFieldValue(duration.querySelector('*[name="duration-time"]'))
        break
      case 'distance':
        unit = parseFieldValue(duration.querySelector('*[name="distanceUnit"]'))
        parsed = parseFieldValue(duration.querySelector('*[name="duration-distance"]'))
        break
      case 'calories':
        parsed = parseFieldValue(duration.querySelector('*[name="duration-calories"]'))
        unit = duration.querySelector('.add-on')
        unit = unit ? unit.innerText : null
        break
      case 'heart.rate.zone':
        parsed = parseFieldValue(duration.querySelector('*[name="duration-heart-rate-custom"]'))
        range = duration.querySelector('*[name="heart-rate-select"]')
        range = parseFieldValue(range.options[range.selectedIndex])
        range.name = 'heart-rate-select'
        unit = duration.querySelector('.add-on')
        unit = unit ? unit.innerText : null
        break
      case 'power.zone':
        parsed = parseFieldValue(duration.querySelector('*[name="duration-power-zone-custom"]'))
        range = duration.querySelector('*[name="power-zone-select"]')
        range = parseFieldValue(range.options[range.selectedIndex])
        range.name = 'power-zone-select'
        unit = duration.querySelector('.add-on')
        unit = unit ? unit.innerText : null
        break
    }
    parsed[KEYS.TYPE] = type
    if (unit) parsed[KEYS.UNIT] = unit
    if (range) parsed[KEYS.RANGE] = range
    return parsed
  }

  function parseTarget(step) {
    var type = step.querySelector('select[name="select-step-target"] option[selected]'),
    from, to, unit, targetType,
    parsed = {}

    if (!type || !type.value) return

    switch(type.value) {
      case 'pace.zone':
        var targetPace = step.querySelector('*[data-input="target-pace"]')
        from = targetPace.querySelector('input[name="target-pace-from"]')
        to = targetPace.querySelector('input[name="target-pace-to"]')
        unit = targetPace.querySelector('.add-on')
        break
      case 'speed.zone':
        var targetSpeed = step.querySelector('*[data-input="target-speed"]')
        from = targetSpeed.querySelector('input[name="target-speed-from"]')
        to = targetSpeed.querySelector('input[name="target-speed-to"]')
        unit = targetSpeed.querySelector('.add-on')
        break
      case 'cadence':
        var targetCadence = step.querySelector('*[data-input="target-cadence"]')
        from = targetCadence.querySelector('input[name="target-cadence-from"]')
        to = targetCadence.querySelector('input[name="target-cadence-to"]')
        unit = targetCadence.querySelector('.add-on')
        break
      case 'heart.rate.zone':
        var targetHR = step.querySelector('*[data-input="target-hr"]')
        targetType = type.dataset.targetType
        from = targetHR.querySelector('input[name="target-heart-rate-custom-from"]')
        to = targetHR.querySelector('input[name="target-heart-rate-custom-to"]')
        unit = targetHR.querySelector('.add-on')
        break
      case 'power.zone':
        var targetPower = step.querySelector('*[data-input="target-power"]')
        targetType = type.dataset.targetType
        console.log('TARGET TYPE', targetType)
        from = targetPower.querySelector('input[name="target-power-zone-custom-from"]')
        to = targetPower.querySelector('input[name="target-power-zone-custom-to"]')
        unit = targetPower.querySelector('.add-on')
        break
    }
    parsed[KEYS.TYPE] = type.value
    if (from) parsed[KEYS.FROM] = parseFieldValue(from)
    if (to) parsed[KEYS.TO] = parseFieldValue(to)
    if (unit && unit.innerText) parsed[KEYS.UNIT] = unit.innerText.trim()
    if (targetType) parsed[KEYS.TARGET_TYPE] = targetType.trim()
    return parsed
  }

  function importWorkout(workout) {
    updateWorkoutName(workout[KEYS.NAME])
    clearWorkoutSteps(document)
    workout[KEYS.STEPS].map(function(step) {
      addStep(step)
    })
    var saveButton = document.querySelector('#save-workout')
    if (saveButton) saveButton.click()
  }

  function updateWorkoutName(name) {
    var form = document.querySelector('#workout-name-edit'),
    editButton = form.querySelector('.inline-edit-trigger'),
    nameField = form.querySelector('.inline-edit-editable-text'),
    saveButton = form.querySelector('.inline-edit-save')

    editButton.click()
    nameField.innerText = name
    saveButton.click()
  }

  function clearWorkoutSteps(context) {
    var steps = context.querySelectorAll('.workout-step .step-delete')
    for (var i = 0; i < steps.length; i++) {
      steps[i].click()
    }
  }

  function removeStep(context) {
    var deleteButton = context.querySelector('.step-delete')
    if (deleteButton) deleteButton.click()
  }

  function addStep(step) {
    if (!step) return

    if (KEYS.TYPE in step) {
      var newStepButton = document.querySelector('#new-step')
      newStepButton.click()
      var form = document.querySelector('.workout-editor > .workout-step:last-of-type')
      updateStep(step, form)
    } else {
      var newRepeatButton = document.querySelector('#new-repeat')
      newRepeatButton.click()

      var form = document.querySelector('.workout-editor > .block-repeat:last-of-type'),
      countIncreaseButton = form.querySelector('*[name="repeat-more"]')
      for (var i = 2; i < step.count; i++) {
        if (countIncreaseButton) countIncreaseButton.click()
      }

      var childrenContainer = form.querySelector('.child-steps'),
      stepPlaceholders = childrenContainer.querySelectorAll('.workout-step')
      for (var i = 0; i < Math.max(stepPlaceholders.length, step[KEYS.STEPS].length); i++) {
        var childStep = step[KEYS.STEPS][i]
        if (i >= stepPlaceholders.length) {
          addStep(childStep)
          var childStepElement = document.querySelector('.workout-editor > .workout-step:last-of-type')
          childrenContainer.appendChild(childStepElement)
        } else {
          if (i < step[KEYS.STEPS].length) {
            updateStep(childStep, stepPlaceholders[i])
          } else {
            removeStep(stepPlaceholders[i])
          }
        }
      }
    }
  }

  function updateStep(step, form) {
    var typeField = form.querySelector('select[name="select-step-type"]'),
    type = typeField.querySelector('option[value="' + ( KEYS.TYPE in step ? step[KEYS.TYPE][KEYS.VALUE] : '' ) + '"]'),
    duration = form.querySelector('select[name="duration"]'),
    target = form.querySelector('select[name="select-step-target"]'),
    field, unit

    if (type) type.selected = true
    typeField.dispatchEvent(new Event('change', { bubbles: true }))

    if (duration) {
      var durationType = duration.querySelector('option[value="' + ( KEYS.DURATION in step ? step[KEYS.DURATION][KEYS.TYPE] : '' ) + '"]')
      if (durationType) durationType.selected = true
      duration.dispatchEvent(new Event('change', { bubbles: true }))

      if (KEYS.NAME in step[KEYS.DURATION]) {
        field = form.querySelector('*[name="' + step[KEYS.DURATION][KEYS.NAME] + '"]')
        if (field) field.value = step[KEYS.DURATION][KEYS.VALUE]
      }
      if (KEYS.UNIT in step[KEYS.DURATION] && typeof step[KEYS.DURATION][KEYS.UNIT] === 'object') {
        unit = form.querySelector('*[name="' + step[KEYS.DURATION][KEYS.UNIT][KEYS.NAME] + '"] option[value="' + step[KEYS.DURATION][KEYS.UNIT][KEYS.VALUE] + '"]')
        if (unit) unit.selected = true
      }

      if (KEYS.RANGE in step[KEYS.DURATION] && typeof step[KEYS.DURATION][KEYS.RANGE] === 'object') {
        unit = form.querySelector('*[name="' + step[KEYS.DURATION][KEYS.RANGE][KEYS.NAME] + '"] option[value="' + step[KEYS.DURATION][KEYS.RANGE][KEYS.VALUE] + '"]')
        if (unit) unit.selected = true
      }
    }

    if (target) {
      var addLink = form.querySelector('#add-step-target')
      if (addLink) addLink.click()
      var targetType = target.querySelector('option[value="' + ( KEYS.TARGET in step ? step[KEYS.TARGET][KEYS.TYPE] : '' ) + '"]'),
      targetFrom, targetTo

      if (step[KEYS.TARGET][KEYS.TYPE] === 'heart.rate.zone' || step[KEYS.TARGET][KEYS.TYPE] === 'power.zone') {
        targetType = target.querySelector('option[value="' + ( KEYS.TARGET in step ? step[KEYS.TARGET][KEYS.TYPE] : '' ) + '"][data-target-type="' + step[KEYS.TARGET][KEYS.TARGET_TYPE] + '"]')
      }

      if (targetType) targetType.selected = true
      target.dispatchEvent(new Event('change', { bubbles: true }))
      if ((step[KEYS.TARGET][KEYS.TYPE] !== 'heart.rate.zone' && step[KEYS.TARGET][KEYS.TYPE] !== 'power.zone') || (step[KEYS.TARGET][KEYS.TARGET_TYPE] && step[KEYS.TARGET][KEYS.TARGET_TYPE] === '-1')) {
        if (KEYS.FROM in step[KEYS.TARGET] && typeof step[KEYS.TARGET][KEYS.FROM] === 'object') {
          targetFrom = form.querySelector('*[name="' + step[KEYS.TARGET][KEYS.FROM][KEYS.NAME] + '"]')
          if (targetFrom) targetFrom.value = step[KEYS.TARGET][KEYS.FROM][KEYS.VALUE]
        }
        if (KEYS.TO in step[KEYS.TARGET] && typeof step[KEYS.TARGET][KEYS.TO] === 'object') {
          targetTo = form.querySelector('*[name="' + step[KEYS.TARGET][KEYS.TO][KEYS.NAME] + '"]')
          if (targetTo) targetTo.value = step[KEYS.TARGET][KEYS.TO][KEYS.VALUE]
        }
      }
    }
  }

  helpers = {
    getURLParam: function(name, url) {
      if (!url) url = window.location.href
      name = name.replace(/[\[\]]/g, "\\$&")
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url)
      if (!results) return null
      if (!results[2]) return ''
      return decodeURIComponent(results[2].replace(/\+/g, " "))
    }
  }

})()
