/**
* Injects JS into the dom.
*/
(function() {
  'use strict'
  var script = document.createElement('script')
  script.src = chrome.extension.getURL('garmin.js')
  script.onload = function() {
    this.remove()
  }
  setTimeout(function() {
    (document.head || document.documentElement).appendChild(script)
  }, 50)

})()
